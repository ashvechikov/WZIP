﻿const getWeatherData = form => {
    try {
        $.ajax({
            type: 'POST',
            url: form.action,
            data: new FormData(form),
            contentType: false,
            processData: false,
            success: function (res) {
                if (res.isValid) {
                    $('#weatherItems').html(res.html);
                    $('#exampleModal').modal('hide');
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
        return false;
    } catch (ex) {
        console.log(ex);
    }
}

function disableChange(sender, buttonId) {
    document.getElementById(buttonId).disabled = sender.value === "";
}