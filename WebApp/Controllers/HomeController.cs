﻿using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WeatherByZipAPI.Data.Entity;
using WeatherByZipAPI.Data.Enum;
using WeatherByZipAPI.Data.Model.Api;
using WeatherByZipAPI.Data.Model.Weather;
using WeatherByZipAPI.Helpers;
using WeatherByZipAPI.Service.WeatherService;
using WebApp.Helpers;
using WebApp.Models;
using WebApp.Models.Configuration;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IWeatherService _weatherService;
        private readonly AppConfig _config;

        public HomeController(IWeatherService weatherService, AppConfig config)
        {
            _weatherService = weatherService;
            _config = config;
        }

        public IActionResult Index()
        {
            return View(_weatherService.GetAll());
        }

        [HttpPost]
        public async Task<IActionResult> FetchWeatherData([Bind] ParamsVm viewModel)
        {
            using var httpClient = new HttpClient();
            var response = await httpClient.GetAsync($"{_config.LocalApi.BaseUrl}?zipCode={viewModel.Zip}&countryCode={viewModel.CountryCode}");
            var responseText = await response.Content.ReadAsStringAsync();
            var apiResponse = JsonConvert.DeserializeObject<ApiResponse>(responseText);
            
            if (apiResponse.Status == ResponseStatus.Success)
            {
                var weatherDataUnit = JsonConvert.DeserializeObject<WeatherDataUnit>(apiResponse.Data);
                if (weatherDataUnit != null)
                {
                    await _weatherService.Save(new Weather(weatherDataUnit.Conditions.Temp, weatherDataUnit.CityName, weatherDataUnit.WeatherItems.First().Main.GetValueFromName<ConditionType>(), DtHelper.GetDateTimeFromUnix(weatherDataUnit.DateTime), weatherDataUnit.WeatherItems.First().Icon));
                }
            }
            
            return Json(new { isValid = true, html = RazorPagesHelper.RenderRazorViewToString(this, "_WeatherItems", _weatherService.GetAll()) });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}