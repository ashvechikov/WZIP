using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WeatherByZipAPI.Repository;
using WeatherByZipAPI.Service.WeatherService;
using WebApp.Models.Configuration;

namespace WebApp
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private ILogger<Startup> Logger { get; set; }
        private AppConfig Config { get; set; }

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            Logger = logger;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            Config = new AppConfig();
            Configuration.Bind("App", Config);
            services.AddSingleton(Config);

            string connString = Configuration.GetConnectionString("main");

            services.AddDbContext<AppContext>(options =>
            {
                if (!string.IsNullOrEmpty(connString))
                {
                    Logger.LogInformation("The database connection initiating ...");
                    options.UseSqlite(connString);
                    Logger.LogInformation("Successfully connected to database!");
                }
                else
                {
                    Logger.LogCritical("Database connection error! The connection string is empty.");
                }
            });

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IWeatherService, WeatherService>();
            services.AddControllersWithViews();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}