﻿namespace WebApp.Models
{
    public class ParamsVm
    {
        public string Zip { get; set; }
        public string CountryCode { get; set; }
    }
}