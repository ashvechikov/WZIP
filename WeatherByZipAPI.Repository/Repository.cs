﻿using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WeatherByZipAPI.Data;

namespace WeatherByZipAPI.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseModel
    {
        private readonly DbSet<TEntity> _dbSet;
        private readonly AppContext _context;
        private readonly ILogger<Repository<TEntity>> _logger;

        public Repository(AppContext context, ILogger<Repository<TEntity>> logger)
        {
            _context = context;
            _logger = logger;
            _dbSet = context.Set<TEntity>();
            _logger.LogInformation($"{GetType()} context for {_dbSet.EntityType} created");
        }
        public IQueryable<TEntity> GetAll()
        {
            _logger.LogInformation($"{GetType()} [{MethodBase.GetCurrentMethod()?.Name}] started");
            return _dbSet.AsNoTracking();
        }
        public void Add(TEntity entity)
        {
            _logger.LogInformation($"{GetType()} [{MethodBase.GetCurrentMethod()?.Name}] started");
            _dbSet.Add(entity);
            _logger.LogInformation($"{GetType()} [{MethodBase.GetCurrentMethod()?.Name}] finished");
        }
        public Task SaveChangesAsync()
        {
            _logger.LogInformation($"{GetType()} [{MethodBase.GetCurrentMethod()?.Name}] started");
            return _context.SaveChangesAsync();
        }
    }
}