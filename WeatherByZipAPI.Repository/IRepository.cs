﻿using System.Linq;
using System.Threading.Tasks;

namespace WeatherByZipAPI.Repository
{
    public interface IRepository<TEntity>
    {
        Task SaveChangesAsync();
        IQueryable<TEntity> GetAll();
        void Add(TEntity entity);
    }
}