﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherByZipAPI.Data.Entity;

namespace WeatherByZipAPI.Service.WeatherService
{
    public interface IWeatherService
    {
        Task Save(Weather e);
        public IEnumerable<Weather> GetAll(int take = 20);
    }
}