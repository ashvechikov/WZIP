﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherByZipAPI.Data.Entity;
using WeatherByZipAPI.Repository;

namespace WeatherByZipAPI.Service.WeatherService
{
    public class WeatherService : IWeatherService
    {
        private readonly IRepository<Weather> _repository;

        public WeatherService(IRepository<Weather> repository)
        {
            _repository = repository;
        }

        public async Task Save(Weather e)
        {
            _repository.Add(e);
            await _repository.SaveChangesAsync();
        }

        public IEnumerable<Weather> GetAll(int take = 20)
        {
            return _repository.GetAll().ToList().OrderByDescending( e=>e.Created).Take(take);
        }
    }
}