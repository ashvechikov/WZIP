#  WeatherByZip
Сlient-server application for searching weather forecast by zip code

## Run

To run the application you need to generate an API key here https://home.openweathermap.org/api_keys

**!!! For convenience, I left my API key in the repository !!! (I know you can't do that :)**

### First you need to launch project "WeatherByZipAPI"
If you want to change the EXTERNAL API settings, you can do it here
**appsettings.Development.json**
```json
"App": {
    "OpenWeatherMapAPI": {
      "BaseUrl": "https://api.openweathermap.org/data/2.5/weather",
      "ApiKey": "2371e9f492f60d40b936208c00390511"
    }
  }
```

### then project "WebApp"

If you want to change the WEB APP settings, you can do it here
**appsettings.Development.json**
```json
"App": {
      "LocalApi": {
      "BaseUrl": "https://localhost:5010/api/weather"
    }
}
```

![1](1.png "1")
![2](2.png "2")