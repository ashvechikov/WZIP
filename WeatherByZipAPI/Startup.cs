using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WeatherByZipAPI.Configuration;

namespace WeatherByZipAPI
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        private AppConfig Config { get; set; }
        private ILogger<Startup> Logger { get; set; }

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            Logger = logger;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            Config = new AppConfig();
            Configuration.Bind("App", Config);
            services.AddSingleton(Config);
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            foreach (var property in Config.OpenWeatherMapApi.GetType().GetProperties())
            {
                if (string.IsNullOrEmpty((string)property.GetValue(Config.OpenWeatherMapApi)))
                    Logger.LogCritical($"Parameter \"{property.Name}\" can not be empty!");
            }

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}