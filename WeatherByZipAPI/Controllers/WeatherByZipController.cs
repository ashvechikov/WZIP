﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WeatherByZipAPI.Configuration;
using WeatherByZipAPI.Data.Enum;
using WeatherByZipAPI.Data.Model.Api;
using WeatherByZipAPI.Data.Model.Weather;
using WeatherByZipAPI.Helpers;

namespace WeatherByZipAPI.Controllers
{
    [ApiController]
    [Route("api")]
    public class WeatherByZipController : ControllerBase
    {
        private WeatherDataUnit _data;
        private readonly AppConfig _config;

        public WeatherByZipController(AppConfig config)
        {
            _config = config;
        }

        [HttpGet]
        [Route("weather")]
        public async Task<IActionResult> Get(uint zipCode, string countryCode, UnitType unit = UnitType.Celsius)
        {
            if (zipCode <= 0)
                return new JsonResult(new ApiResponse(ResponseStatus.WrongParameter, null));

            if (string.IsNullOrEmpty(countryCode))
                return new JsonResult(new ApiResponse(ResponseStatus.ParameterNotSpecified, null));

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync($"{_config.OpenWeatherMapApi.BaseUrl}?zip={zipCode},{countryCode}&units={unit.GetDisplayName()}&appid={_config.OpenWeatherMapApi.ApiKey}");

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    _data = JsonConvert.DeserializeObject<WeatherDataUnit>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    return new JsonResult(new ApiResponse(ResponseStatus.ExternalApiError, null));
                }
            }

            return new JsonResult(new ApiResponse(ResponseStatus.Success, JsonConvert.SerializeObject(_data)));
        }
    }
}