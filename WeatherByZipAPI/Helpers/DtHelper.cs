﻿using System;

namespace WeatherByZipAPI.Helpers
{
    public static class DtHelper
    {
        public static DateTime GetDateTimeFromUnix(ulong stamp)
        {
            var start = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return start.AddSeconds(stamp).ToLocalTime();
        }
    }
}