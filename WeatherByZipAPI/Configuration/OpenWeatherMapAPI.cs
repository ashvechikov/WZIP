﻿namespace WeatherByZipAPI.Configuration
{
    public class OpenWeatherMapAPI
    {
        public string BaseUrl { get; set; }
        public string ApiKey { get; set; }
    }
}