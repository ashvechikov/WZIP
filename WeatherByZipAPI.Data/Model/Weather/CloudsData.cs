﻿using Newtonsoft.Json;

namespace WeatherByZipAPI.Data.Model.Weather
{
    public class CloudsData
    {
        [JsonProperty("all")]
        public byte All { get; set; }
    }
}