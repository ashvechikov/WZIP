﻿using Newtonsoft.Json;

namespace WeatherByZipAPI.Data.Model.Weather
{
    public class WindData
    {
        [JsonProperty("speed")]
        public float Speed { get; set; }

        [JsonProperty("deg")]
        public float Deg { get; set; }

        [JsonProperty("gust")]
        public float Gust { get; set; }
    }
}