﻿using Newtonsoft.Json;

namespace WeatherByZipAPI.Data.Model.Weather
{
    public class Location
    {
        [JsonProperty("lon")]
        public float Lon { get; set; }

        [JsonProperty("lat")]
        public float Lat { get; set; }
    }
}