using System.Collections.Generic;
using Newtonsoft.Json;

namespace WeatherByZipAPI.Data.Model.Weather
{
    public class WeatherDataUnit
    {
        [JsonProperty("coord")]
        public Location Location { get; set; }

        [JsonProperty("main")]
        public ConditionsData Conditions { get; set; }

        [JsonProperty("wind")]
        public WindData Wind { get; set; }

        [JsonProperty("clouds")]
        public CloudsData Clouds { get; set; }

        [JsonProperty("sys")]
        public ExtraData Extra { get; set; }
        
        [JsonProperty("weather")]
        public IEnumerable<WeatherItem> WeatherItems { get; set; }

        [JsonProperty("visibility")]
        public uint Visibility { get; set; }

        [JsonProperty("name")]
        public string CityName { get; set; }

        [JsonProperty("id")]
        public int CityId { get; set; }

        [JsonProperty("timezone")]
        public int Timezone { get; set; }
        
        [JsonProperty("dt")]
        public ulong DateTime { get; set; }
        
        [JsonProperty("base")]
        public string Base { get; set; }
        
        [JsonProperty("cod")]
        public int Cod { get; set; }
    }
}