﻿using Newtonsoft.Json;

namespace WeatherByZipAPI.Data.Model.Weather
{
    public class ExtraData
    {
        [JsonProperty("type")]
        public int Type { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("sunrise")]
        public ulong Sunrise { get; set; }

        [JsonProperty("sunset")]
        public ulong Sunset { get; set; }
    }
}