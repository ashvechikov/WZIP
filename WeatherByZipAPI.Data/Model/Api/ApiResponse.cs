using Newtonsoft.Json;
using WeatherByZipAPI.Data.Enum;

namespace WeatherByZipAPI.Data.Model.Api
{
    public class ApiResponse
    {
        public ApiResponse(ResponseStatus status, string data)
        {
            Status = status;
            Data = data;
        }

        [JsonProperty("status")]
        public ResponseStatus Status { get; set; }

        [JsonProperty("data")]
        public string Data { get; set; }
    }
}