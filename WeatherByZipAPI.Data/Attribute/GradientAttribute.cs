﻿namespace WeatherByZipAPI.Data.Attribute
{
    public class GradientAttribute : System.Attribute
    {
        public GradientAttribute(string firstColor, string secondColor)
        {
            From = firstColor.Contains("#") ? firstColor : $"#{firstColor}";
            To = secondColor.Contains("#") ? secondColor : $"#{secondColor}";
        }
        public string From { get; set; }
        public string To { get; set; }
    }
}