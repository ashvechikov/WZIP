﻿using System.ComponentModel.DataAnnotations;
using WeatherByZipAPI.Data.Attribute;

namespace WeatherByZipAPI.Data.Enum
{
    public enum ConditionType
    {
        [Gradient("#4B79A1", "#283E51")]
        [Display(Name = "Thunderstorm", Description = "thunderstorm")]
        Thunderstorm,

        [Gradient("#E8CBC0", "#636FA4")]
        [Display(Name = "Drizzle", Description = "weather_mix")]
        Drizzle,

        [Gradient("#2C3E50", "#4CA1AF")]
        [Display(Name = "Rain", Description = "rainy")]
        Rain,

        [Gradient("#E8CBC0", "#636FA4")]
        [Display(Name = "Snow", Description = "ac_unit")]
        Snow,

        [Gradient("#bdc3c7", "#2c3e50")]
        [Display(Name = "Mist", Description = "mist")]
        Mist,

        [Gradient("#ffd89b", "#19547b")]
        [Display(Name = "Smoke", Description = "heat")]
        Smoke,

        [Gradient("#8e9eab", "#eef2f3")]
        [Display(Name = "Haze", Description = "mist")]
        Haze,

        [Gradient("#eacda3", "#d6ae7b")]
        [Display(Name = "Dust", Description = "snowing_heavy")]
        Dust,

        [Gradient("#E8CBC0", "#636FA4")]
        [Display(Name = "Fog", Description = "foggy")]
        Fog,

        [Gradient("#E8CBC0", "#636FA4")]
        [Display(Name = "Sand", Description = "snowing_heavy")]
        Sand,

        [Gradient("#E0EAFC", "#CFDEF3")]
        [Display(Name = "Ash", Description = "heat")]
        Ash,

        [Gradient("#E8CBC0", "#636FA4")]
        [Display(Name = "Squall", Description = "air")]
        Squall,

        [Gradient("#1D4350", "#A43931")]
        [Display(Name = "Tornado", Description = "tornado")]
        Tornado,

        [Gradient("#4CA1AF", "#C4E0E5")]
        [Display(Name = "Clear", Description = "clear_day")]
        Clear,

        [Gradient("#E8CBC0", "#636FA4")]
        [Display(Name = "Clouds", Description = "cloud")]
        Clouds
    }
}