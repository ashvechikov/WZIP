﻿using System.ComponentModel.DataAnnotations;

namespace WeatherByZipAPI.Data.Enum
{
    public enum UnitType
    {
        [Display(Name = "metric")]
        Celsius = 0,
        
        [Display(Name = "standard")]
        Kelvin,

        [Display(Name = "imperial")]
        Fahrenheit
    }
}