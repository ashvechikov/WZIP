﻿using System.ComponentModel.DataAnnotations;

namespace WeatherByZipAPI.Data.Enum
{
    public enum ResponseStatus
    {
        [Display(Name = "success")]
        Success = 0,

        [Display(Name = "parameter not specified", Description = "Parameter not specified")]
        ParameterNotSpecified = 1,

        [Display(Name = "wrong parameter", Description = "Wrong parameter")]
        WrongParameter = 2,

        [Display(Name = "external error", Description = "External service error occured")]
        ExternalApiError = 3
    }
}