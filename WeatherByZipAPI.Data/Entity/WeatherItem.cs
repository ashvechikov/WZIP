﻿using System;
using WeatherByZipAPI.Data.Enum;

namespace WeatherByZipAPI.Data.Entity
{
    public class Weather : BaseModel
    {
        public Weather(float temperature, string city, ConditionType conditionType, DateTime dateTimeReq, string icon)
        {
            Temperature = temperature;
            City = city;
            ConditionType = conditionType;
            DateTimeReq = dateTimeReq;
            Icon = icon;
        }

        public float Temperature { get; set; }
        public string City { get; set; }
        public ConditionType ConditionType { get; set; }
        public DateTime DateTimeReq { get; set; }
        public string Icon { get; set; }
    }
}