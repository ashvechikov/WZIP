﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WeatherByZipAPI.Data
{
    public class BaseModel
    {
        protected BaseModel()
        {
            Id = Guid.NewGuid().ToString();
            Created = Updated = DateTime.Now;
        }

        [Key]
        public string Id { get; set; }

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}